package main

// multi-parameter func in Go
func PlusInGo(x, y int) int {
	return x + y
}
