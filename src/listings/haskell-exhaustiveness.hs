module Dolphin where

data Message = DoAFlip | Fish | Ohai

dolphin :: Message -> String
dolphin msg = case msg of
  DoAFlip -> "How about no?"
  Fish    -> "So long and thanks for all the fish!"
